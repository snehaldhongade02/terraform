provider "aws"{
  region = var.region
}

resource "aws_instance" "my_instance" {
  ami = "ami-0f98860b8bc09bd5c"
  instance_type = var.instance_type
  key_name = var.key-pair
  tags = var.tags
  vpc_security_group_ids = var.sg_ids
}
variable "region" {
  description = "please enter your aws region"
  default = "ap-southeast-1"
}

variable "key-pair" {
  default = "jenkins-key"
}

variable "instance_type" {
  description = "instance type"
  default = "t3.micro"
}

variable "tags" {
  type = map
  default = {
     env = "dev"
     Name = "my-instance"
  }
}

variable "sg_ids" {
  type = list
  default = [
  "sg-08f5ce86ddc0c451d"
  ]
 }